# otter spark example

# 환경 설정
- 처음 프로젝트를 받으면 다음 명령을 수행하여 spark 실행에 필요한 파일을 설치하세요. 
```
$ bin/setup.sh
```

## local 환경에서 실행
```
$ bin/run-02-local.sh
```

## cluster 환경에서 실행 
- spark-yarn cluster
```
$ bin/run-03-cluster.sh
```

# 클러스터 설정 관리
## yarn configuration 관리
- Cloudera Manager -> YARN -> Actions -> Download Client Configuration -> yarn-clientconfig.zip
- 프로젝트에서는 $root/conf/cluster-01/yarn-conf 디렉토리에 내려받은 설정파일 추가

```
export YARN_CONF_DIR=$root/conf/cluster-01/yarn-conf

SPARK_APP_JAR=$root/target/scala-2.11/spark-app.jar
SPARK_HOME=$root/download/tool/spark
${SPARK_HOME}/bin/spark-submit \
  --class spark.tutorial.SparkAppExample \
  --master yarn \
  ${SPARK_APP_JAR} args1 args2 args3

```
