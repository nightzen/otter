name := "otter-spark-tutorial"
version := "18.07.1"

val versions = new {
  val scala = "2.11.12"
  val spark = "1.6.3"
  val hadoop = "2.6.0"
  val typesafeConfig = "1.3.3"
}

scalaVersion := versions.scala
logLevel := Level.Warn
logLevel in assembly := Level.Error

lazy val root = (project in file(".")).
  settings(
    version := "0.1.0",
    organization := "spark.tutorial"
  )

val dependencyScope = "compile"

libraryDependencies ++= Seq(
  "com.typesafe" % "config" % versions.typesafeConfig,
  "org.apache.spark" %% "spark-core" % versions.spark % dependencyScope exclude("org.apache.hadoop", "*"),
  "org.apache.spark" %% "spark-sql" % versions.spark % dependencyScope exclude("org.apache.hadoop", "*"),
  "org.apache.hadoop" % "hadoop-mapreduce-client-core" % versions.hadoop % dependencyScope,
  "org.apache.hadoop" % "hadoop-common" % versions.hadoop % dependencyScope,
  "org.apache.hadoop" % "hadoop-hdfs" % versions.hadoop % dependencyScope
)

// to include *.conf file for package
resourceDirectory := baseDirectory.value / "src/main/scala"

// TODO
assemblyMergeStrategy in assembly := {
//  case PathList("org.datasyslab", "geospark", xs@_*) => MergeStrategy.first
  case PathList("META-INF", "MANIFEST.MF") => MergeStrategy.discard
  case path if path.endsWith(".SF") => MergeStrategy.discard
  case path if path.endsWith(".DSA") => MergeStrategy.discard
  case path if path.endsWith(".RSA") => MergeStrategy.discard
  case _ => MergeStrategy.first
}

assemblyJarName in assembly := "spark-app.jar"