package spark.tutorial

import org.apache.spark.{SparkConf, SparkContext}

object SparkAppLocalExample {
  val appName = "SparkAppExample"
  val conf = new SparkConf().setMaster("local").setAppName(appName)
  val sc = new SparkContext(conf)

  def main(args: Array[String]) {
    println(s"# arguments : ${args.mkString(" ")}")

    val rdd = sc.parallelize(Seq(1, 2, 3, 5, 6))
    val count = rdd.count()
    println(s"count = $count")
  }
}
