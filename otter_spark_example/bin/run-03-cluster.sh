#!/usr/bin/env bash
root=$(cd $(dirname $(readlink $0 || echo $0))/..;/bin/pwd)

cd $root
./sbt clean assembly

export YARN_CONF_DIR=$root/conf/cluster-01/yarn-conf

SPARK_APP_JAR=$root/target/scala-2.11/spark-app.jar
SPARK_HOME=$root/download/tool/spark
${SPARK_HOME}/bin/spark-submit \
  --class spark.tutorial.SparkAppClusterExample \
  --master yarn \
  ${SPARK_APP_JAR} args1 args2 args3
