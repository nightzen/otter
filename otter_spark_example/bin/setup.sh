#!/usr/bin/env bash
root=$(cd $(dirname $(readlink $0 || echo $0))/..;/bin/pwd)

mkdir -p $root/download/tool
cd $root/download/tool

if [ ! -f spark-1.6.3-bin-hadoop2.6.tgz ]
then
    curl https://archive.apache.org/dist/spark/spark-1.6.3/spark-1.6.3-bin-hadoop2.6.tgz > spark-1.6.3-bin-hadoop2.6.tgz
fi

if [ ! -d spark-1.6.3-bin-hadoop2.6 ]
then
    tar xvfz spark-1.6.3-bin-hadoop2.6.tgz
    ln -s spark-1.6.3-bin-hadoop2.6 spark
fi